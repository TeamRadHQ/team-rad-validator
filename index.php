<?php
// Define namespace aliases
use \TeamRad\Helpers\Opt as Opt;
use \TeamRad\Helpers\Cnd as Cnd;
use TeamRad\Form\Fieldsets as Fields;

// Autoloader
require_once('./vendor/autoload.php');

add_head('PB Test');
?>
<body class="container">

<div class="row container text-right">
	<a class="btn btn-primary" title="Example">Form Example</a>
	<a class="btn btn-default" href="./tests.php" title="View Validation Tests">Validator Tests</a>
</div>

<h1>Tutoring Application</h1>

<?php
$form = new TeamRad\Form\Form;
$name = new Fields\Name();
$birthday = new \TeamRad\Form\Field('birthday');
$birthday->set_label('Date of Birth');
$birthday->set_message('Please enter your DOB in DD/MM/YYYY format.');
$name->add_field($birthday);
$form->set_fieldset($name);
$form->set_fieldset(new Fields\VUContact());
$form->set_fieldset(new Fields\Address());
$form->set_fieldset(new Fields\Tutoring());

$form->set_POST_values();	
$form->render();

// Load scripts
add_js();
?>

</body>
</html>