<!-- <pre> -->
<?php
// Define namespace aliases
use \TeamRad\Helpers\Opt as Opt;
use \TeamRad\Helpers\Cnd as Cnd;
use TeamRad\Form\Fieldsets as Fields;
//AutoLoad Classes
require_once('./vendor/autoload.php');
add_head('Validation Tests');
?>
<body class="container">
<div class="row container text-right">
	<a class="btn btn-default" href="./" title="Example">Form Example</a>
	<a class="btn btn-primary" title="View Validation Tests">Validator Tests</a>
</div>
<h1>Test Types and Conditions</h1>
<?php
$form = new TeamRad\Form\Form;
$types = new Fields\TestTypes();
$condition = new Fields\TestConditions();
$form->set_fieldset($types);
$form->set_fieldset($condition);
$form->set_POST_values();	
$form->render();

add_js();
?>
</body>
</html>