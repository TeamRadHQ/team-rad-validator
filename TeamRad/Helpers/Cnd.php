<?php 
namespace TeamRad\Helpers;

/**
 * Returns an option, value array.
 * This can be used to add options for select fields, radio
 * buttons and checklists.
 */
class Cnd {
	/**
	 * Condition (CND) Adders
	 */
	public static function min($val) {
		return array("min_value" => $val);
	}
	public static function max($val) {
		return array("max_value" => $val);
	}
	public static function btwval($val1, $val2) {
		$vals = self::minmax($val1, $val2);	
		return array_merge(
			self::min($vals["min"]),
			self::max($vals["max"]));
	}
	public static function pos() {
		return array("is_postive" => true);
	}
	public static function neg() {
		return array("is_negative" => true);
	}
	public static function len($val) {
		return array("exact_length" => $val);
	}
	public static function minlen($val) {
		return array("min_length" => $val);
	}
	public static function maxlen($val) {
		return array("max_length" => $val);
	}
	public static function btwlen($val1, $val2) {
		$vals = self::minmax($val1, $val2);	
		return array_merge(
			minlen($vals["min"]),
			maxlen($vals["max"]));
	}
	public static function contains($val) {
		return array("str_contains" => $val);
	}
	public static function not($val) {
		return array("str_not_contains" => $val);
	}
	public static function build($array) {
		$conditions=array();
		foreach(array_keys($array) as $key) {
			$conditions = array_merge($conditions, $array[$key]);
		}
		return $conditions;
	}

	private static function minmax($val1, $val2) {
		if ($val1 < $val2 || $val1 == $val2) { //Val1 is min
			return array("min" => $val1, "max" => $val2);
		} else { // Val2 is min
			return array("min" => $val2, "max" => $val1);
		}
	}// end minmax()

} // End class
?>
