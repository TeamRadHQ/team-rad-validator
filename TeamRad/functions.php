<?php

/**
 * Returns $prefix with an underscore appended.
 * @param  string $prefix The prefix.
 * @return string         The prefix with an underscore.
 */
function prefix($prefix=null) {
		if($prefix)
			$prefix.="_";
		return $prefix;
}
/**
 * Takes two values (numeric), sorts them as "min" and "max"
 * and returns these in an array.
 * @param  int|float $val1 The first value you want to sort.
 * @param  int|float $val2 The second value you want to sort. 
 * @return array       	An array containing ["min", "max"].
 */
function minmax($val1, $val2) {
	if ($val1 < $val2 || $val1 == $val2) { //Val1 is min
		return array("min" => $val1, "max" => $val2);
	} else { // Val2 is min
		return array("min" => $val2, "max" => $val1);
	}
}// end minmax()

/**
 * Adds the HTML page header.
 * @param string $title The <title> you want to set for 
 * the page.
 */
function add_head($title) {
	echo $head = <<<EOD
<head>

	<!-- Start BootStrap -->
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		
		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet">
	<!-- End BootStrap -->
	<title>{$title}</title>
	<link rel="stylesheet" href="style.css">
</head>
EOD;
}

/**
 * Echos the Required JavaScripts for this project.
 * Place this at the end of the document.
 */
function add_JS($js="") {
	echo $js= <<<EOD

EOD;
}
?>
