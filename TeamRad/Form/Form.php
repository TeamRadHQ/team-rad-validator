<?php
namespace TeamRad\Form;
use TeamRad\Helpers;
use Mustache_Loader_FilesystemLoader;
/**
 * TeamRad/Form is a class which can be used to create a 
 * fieldset, render a form, collect user input, validate 
 * its data and provide information back to the user. 
 * The form is generated using the Mustache template 
 * engine. TeamRad\Form uses the Fieldset class for storing
 * field data.
 */
class Form {
	/**
	 * An array which contains the entire form contents. This 
	 * array is sent to Mustache, which renders the form to the 
	 * browser.
	 * @var array
	 */
	protected $form = array();
	/**
	 * An array of fieldset objects. Each of these fieldsets is
	 * rendered to the browser in the order they are added to 
	 * this array.
	 * @var array
	 */
	public $fieldsets = array();
	/**
	 * Contains the Mustache Template object. Use this object to 
	 * pass arrays to Mustache that are rendered to the browser.
	 * @var \Mustache\Mustache_Engine
	 */
	protected $templates;
	/**
	 * A count of total validation errors 
	 * @var int
	 */
	protected $error_count;
	/**
	 * The constructor instantiates a new Mustache template object.
	 * @todo Add ability to pass options to constructor.
	 */
	public function __construct(array $options = array()) {
		extract($options);
		$this->form["action"] = (isset($action) ? $action : '');
		$this->form["method"] = (isset($method) ? $method : 'POST');
		$this->loadTemplates();
	} // end __construct()

	/**
	 * Takes a fieldset object and adds or updates it.
	 * @param FieldSet $fieldset An instantiated FieldSet object.
	 */
	public function set_fieldset($fieldset) {
		$this->fieldsets[$fieldset->label()] = $fieldset;
  	} // end set_fieldset;
  	/**
  	 * Loops through each fieldset and calls its set_POST_values()
  	 * method. 
  	 */
  	public function set_POST_values() {
  		foreach(array_keys($this->fieldsets) as $set) {
  			$this->fieldsets[$set]->set_POST_values();
  			$this->error_count += $this->fieldsets[$set]->error_count();
  		}// end foreach($set)
  	}// end set_POST_values()
  	/**
  	 * Sets the form's method and action. Loads the form's fieldset. 
  	 * Adds a "success" flag, if there is form input and no validation 
  	 * errors.
  	 */
	public function set_form() {
		foreach($this->fieldsets as $set => $object) {
			$this->form["groups"][] = $this->fieldsets[$set]->get();
		}
		if(count($_POST) >1 && !$this->error_count) 
			$this->form["success"] = true;
		return $this->form;
	} // end set_form()
	protected function loadTemplates() {
		// Load Mustache templates
		$this->templates = 	new \Mustache_Engine([
		'loader' => new Mustache_Loader_FilesystemLoader(__DIR__.'/templates'),
		]);  
	}
	/**
	 * This form compiles the form data and renders it to the browser 
	 * using the Mustache template named /templates/form.mustache
	 * @return string Echoes html to browser.
	 */
  	public function render() {
  		$this->set_form();
  		echo $this->templates->render('form', array("form" => $this->form));
  	}
} // End class Form
