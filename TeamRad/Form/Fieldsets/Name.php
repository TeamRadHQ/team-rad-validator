<?php
namespace TeamRad\Form\Fieldsets;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
use TeamRad\Form\Fieldset as FieldSet;
use TeamRad\Form\Fieldsets\Field as Field;

/**
 * The Name class extends FieldSet. It simplifies the 
 * creation of a FieldSet for collecting a person's name. 
 * By default, both first and last name are required, title
 * is optional.
 */
class Name extends FieldSet {
	public function __construct($prefix="", $required=false) {
		$ucprefix = ucwords($prefix);
		$prefix = prefix($prefix);
		// Set and format the label
		$label = 'Name';
		if ($ucprefix) 
			$label = $ucprefix.' '.$label;
		$this->set_label('', $label);
		$title = new Field\NameTitle($prefix);
		$title->set_required(false);
		$first_name = new Field\NameFirst($prefix);
		$last_name = new Field\NameLast($prefix);
		$this->add_field($title); 
		$this->add_field($first_name); 
		$this->add_field($last_name); 
		// Call the parent constructor.
		parent::__construct();
		$this->set_tmpl_input('fieldset_name');
	} // end __construct()
} // end class 	
?>	
