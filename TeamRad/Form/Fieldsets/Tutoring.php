<?php
namespace TeamRad\Form\Fieldsets;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
use TeamRad\Form\Field as Field;
use TeamRad\Form\Fieldsets\Field as Fields;

/**
 * The Address class extends FieldSet. It simplifies the 
 * creation of an address FieldSet object by creating the 
 * required fields. These fields' ids can be prefixed by
 * passing $prefix when the Address object is instantiated. 
 */
class Tutoring extends \TeamRad\Form\FieldSet {
	public function __construct($prefix="") {
		$ucprefix = ucwords($prefix);
		// Set and format the label
		$label = 'Tutoring';
		if ($ucprefix) 
			$label = $ucprefix.' '.$label;
		$this->set_label('', $label);
		// Format prefix for ids
		$prefix = prefix($prefix);
		// Define Fields
		// Preferred Hours
		$hours = new Field('hours');
		$hours->set_type('int');
		$hours->cnd_btwval(1, 4);
		$hours->set_message('Sessions can run between 1 and 4 hours.');
		$hours->set_placeholder('Enter your preferred hours...');
		
		// Skill Level Field
		$skill = new Field('skill');
		$skill->set_type('select');
		$skill->set_label('Skill Level');
		$skill->set_placeholder('Please indicate your level of proficiency...');
		$skill->set_options(Opt::skill_lvl());
		$skill->set_message('Select at least one preferred day.');
		$days = new Field('days');
		$days->set_label('Preferred Days');
		$days->set_type('checkbox');
		$days->set_options(Opt::busweek());
		// Add fields
		$this->add_field($hours);
		$this->add_field($skill);
		$this->add_field($days);
		parent::__construct();

	} // end __construct()
} // end class 	
?>	
