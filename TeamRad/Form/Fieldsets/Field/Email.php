<?php
namespace TeamRad\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
/**
 * Use this to create an email field object.
 */
class Email extends \TeamRad\Form\Field {
	public function __construct($prefix="", $suffix="") {
		// Add address field
		// Call the parent constructor.
		parent::__construct($prefix.'email'.$suffix);
		$this->set_type('email');
		$this->set_label('Email');
		$this->set_placeholder('Your email...');
	} // end __construct()
} // end class 	
?>	
