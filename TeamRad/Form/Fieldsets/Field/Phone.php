<?php
namespace TeamRad\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;

/**
 * Use this to create a numeric object that takes an 
 * unformatted eight to ten digit Australian phone number.
 */
class Phone extends \TeamRad\Form\Field {
	public function __construct($prefix="", $suffix="") {
		// Add address field
		// Call the parent constructor.
		parent::__construct($prefix.'phone'.$suffix);
		$this->set_type('numeric');
		$this->set_label('Phone');
		$this->set_placeholder('Your phone number...');
		$this->cnd_btwlen(8,13);
	} // end __construct()
} // end class 	
?>	
