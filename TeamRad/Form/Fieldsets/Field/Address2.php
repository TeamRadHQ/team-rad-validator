<?php
namespace TeamRad\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
/**
 *  Use this to create a field object for address line 2
 */
class Address2 extends Address1 {
	public function __construct($prefix="") {
		// Add address field
		// Call the parent constructor.
		parent::__construct($prefix, '_2');
		$this->set_label(' ');
		$this->set_placeholder(' ');
		$this->set_required(false);
	} // end __construct()
} // end class 	
?>	
