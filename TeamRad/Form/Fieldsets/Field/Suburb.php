<?php
namespace TeamRad\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;

/**
 * Use this to create a text_spaces field object for 
 * storing an Australian suburb.
 */
class Suburb extends \TeamRad\Form\Field {
	public function __construct($prefix="") {
		// Add address field
		// Call the parent constructor.
		parent::__construct($prefix.'suburb');
		$this->set_label('Suburb');
		$this->set_placeholder('Your suburb...');
		$this->cnd_maxlen(35);
	} // end __construct()
} // end class 	
?>	
