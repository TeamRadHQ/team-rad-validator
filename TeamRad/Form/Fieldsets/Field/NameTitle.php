<?php
namespace TeamRad\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
use TeamRad\Form\Field as Field;
/**
 * Use this to create a select field object with options 
 * for common western honorific titles. 
 */
class NameTitle extends Field {
	public function __construct($prefix="") {
		// Call the parent constructor.
		parent::__construct($prefix.'title');
		$this->set_label('Title');
		$this->set_required(false);
		$this->set_type('select');
		$this->set_placeholder('Title');
		$this->set_options(Opt::titles());
	} // end __construct()
} // end class 	
?>	
