<?php
namespace TeamRad\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
use TeamRad\Helpers as Helpers;


/**
 * Use this to create a select field object with options 
 * for Australian states.
 */
class State extends \TeamRad\Form\Field {
	public function __construct($prefix="") {

		// Add state field
		parent::__construct($prefix.'state');
		$this->set_label('State');
		$this->set_type('select');
		$this->set_placeholder('Please select a state');
		$this->set_options(Opt::states());
		// Call the parent constructor.

	} // end __construct()
} // end class 	
?>	
