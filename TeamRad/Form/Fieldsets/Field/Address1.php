<?php
namespace TeamRad\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
/**
 * Use this to create a field object for address line 1
 */
class Address1 extends \TeamRad\Form\Field {
	public function __construct($prefix="", $suffix="") {
		// Add address field
		// Call the parent constructor.
		parent::__construct($prefix.'address'.$suffix);
		$this->set_label('Address');
		$this->set_placeholder('Your address...');
		$this->cnd_maxlen(50);
	} // end __construct()
} // end class 	
?>	
