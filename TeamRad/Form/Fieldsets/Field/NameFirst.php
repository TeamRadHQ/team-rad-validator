<?php
namespace TeamRad\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
/**
 * Use this to create a first name field object.
 */
class NameFirst extends \TeamRad\Form\Field {
	public function __construct($prefix="", $name_id='first') {
		// Call the parent constructor.
		parent::__construct($prefix.$name_id.'_name');
		$this->set_label('First Name');
		$this->set_type('text_spaces');
		$this->set_placeholder('Your first name...');
		$this->cnd_maxlen(35);
	} // end __construct()
} // end class 	
?>	
