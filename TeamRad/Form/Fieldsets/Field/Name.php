<?php
namespace TeamRad\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
use TeamRad\Form\Field as Field;
/**
 * Use this to create a name field object.
 */
class Name extends Field {
	public function __construct($prefix="") {
		// Call the parent constructor.
		parent::__construct($prefix.'name');
		$this->set_label('Name');
		$this->set_type('text_spaces');
		$this->set_placeholder('Your name...');
		$this->cnd_maxlen(35);
	} // end __construct()
} // end class 	
?>	
