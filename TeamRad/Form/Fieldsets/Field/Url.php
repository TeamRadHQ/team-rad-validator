<?php
namespace TeamRad\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;

/**
 * Use this for creating a url field object for storing
 * a valid web address.
 */
class Url extends \TeamRad\Form\Field {
	public function __construct($prefix="", $suffix="") {
		// Add address field
		// Call the parent constructor.
		parent::__construct($prefix.'url'.$suffix);
		$this->set_type('url');
		$this->set_label('Website');
		$this->set_placeholder('Enter a url...');
	} // end __construct()
} // end class 	
?>	
