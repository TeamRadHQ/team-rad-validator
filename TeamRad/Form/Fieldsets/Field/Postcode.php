<?php
namespace TeamRad\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;

/**
 * Use this to create an integer field for storing a four
 * digit Australian postcode. 
 */
class Postcode extends \TeamRad\Form\Field {
	public function __construct($prefix="") {
		// Add postcode field
		parent::__construct($prefix.'postcode');
		$this->set_label('Postcode');
		$this->set_type('int');
		$this->set_placeholder('Your postcode...');
		$this->cnd_len(4);
		$this->cnd_pos(true);
	} // end __construct()
} // end class 	
?>	
