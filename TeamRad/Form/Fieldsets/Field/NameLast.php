<?php
namespace TeamRad\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
/**
 * Use this to create a last name field object.
 */
class NameLast extends NameFirst {
	public function __construct($prefix="") {
		// Call the parent constructor.
		parent::__construct($prefix, 'last');
		$this->set_label('Last Name');
		$this->set_placeholder('Your last name...');
	} // end __construct()
} // end class 	
?>	
