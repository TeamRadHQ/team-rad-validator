<?php
namespace TeamRad\Form\Fieldsets;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;

/**
 * The Test class extends FieldSet. This fieldset outputs
 * a set of fields which can be used for testing conditional
 * validation. 
 * All numeric field conditions are set to 10.
 * All string tests are set to match 'test'.  
 */
class TestConditions extends \TeamRad\Form\FieldSet {
	public function __construct() {
		$this->set_label('', 'Conditional Validation Tests');
//'min_value',
		$this->set_field('min_value');
		$this->set_type('min_value','float');
		$this->cnd_minval('min_value', 10);
		$this->set_message('min_value', 'Please enter a numeric value of 10 or higher');
//'max_value',
		$this->set_field('max_value');
		$this->set_type('max_value', 'float');
		$this->cnd_maxval('max_value', 10);
		$this->set_message('max_value', 'Please enter a value of 10 or lower.');
//'max_value',
		$this->set_field('btw_val');
		$this->set_type('btw_val', 'float');
		$this->cnd_btwval('btw_val', 10, -10);
		$this->set_message('btw_val', 'Please enter a value between -10 and 10.');
//'is_positive',
		$this->set_field('is_postive');
		$this->set_type('is_postive', 'float');
		$this->cnd_pos('is_postive');
		$this->set_message('is_postive', 'Please enter a positive number.');
//'is_negative',
		$this->set_field('is_negative');
		$this->set_type('is_negative', 'float');
		$this->cnd_neg('is_negative');
		$this->set_message('is_negative', 'Please enter a negative number.');
//'min_length',
		$this->set_field('min_length');
		$this->cnd_minlen('min_length', 5);
		$this->set_message('min_length', 'Please enter a string of more than 10 characters.');
//'max_length',
		$this->set_field('max_length');
		$this->cnd_maxlen('max_length', 5);
		$this->set_message('max_length', 'Please enter a string of no more than 10 characters.');
//'btw_length',
		$this->set_field('btw_length');
		$this->cnd_btwlen('btw_length', 10, 5);
		$this->set_message('btw_length', 'Please enter a string of between 5 and 10 characters.');
//'exact_length',
		$this->set_field('exact_length');
		$this->cnd_len('exact_length', 5);
		$this->set_message('exact_length', 'Please enter a string of exactly 10 characters.');
//'str_contains',
		$this->set_field('str_contains');
		$this->cnd_contains('str_contains', 'test');
		$this->set_message('str_contains', 'Please enter a string that contains the sub-string "test".');
//'str_not_contains'
		$this->set_field('str_not_contains');
		$this->cnd_not('str_not_contains', 'test');
		$this->set_message('str_not_contains', 'Please enter a string that does not contain the sub-string "test".');
		// Call the parent constructor.
		// echo "<pre>";
		// print_r($this->get('str_not_contains'));
		// echo "</pre>";	
		parent::__construct();
	} // end __construct()
} // end class 	
?>	
