<?php
namespace TeamRad\Form\Fieldsets;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
use TeamRad\Form\Fieldsets\Field as Field;

/**
 * The ContactInfo class extends FieldSet. It creates 
 * a fieldset for storing contact information. There are
 * two telephone and email fields respectively, and a field
 * for a web address. By default, only the first phone and 
 * email field are required, all others optional.
 */
class VuContact extends \TeamRad\Form\FieldSet {
	public function __construct($prefix="") {
		$ucprefix = ucwords($prefix);
		// Set and format the label
		$label = 'Contact Information';
		if ($ucprefix) 
			$label = $ucprefix.' '.$label;
		$this->set_label('', $label);
		// Format prefix for ids
		$prefix = prefix($prefix);
		// Create Phone Number Fireld
		$phone1 = new Field\Phone($prefix.'1');
		$phone1->set_label('Primary Phone');
		$email1 = new Field\Email('email_vu');
		$email1->set_label('VU Email');
		$email1->cnd_contains('vu.edu.au');
		$email2 = new Field\Email($prefix.'2');
		$email2->set_label('Alt Email');
		$email2->set_required(false);
		$email2->cnd_not('vu.edu.au');
		$this->add_field($phone1);
		$this->add_field($email1);
		$this->add_field($email2);
		
		parent::__construct();

	} // end __construct()
} // end class 	
?>	
