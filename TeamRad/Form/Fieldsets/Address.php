<?php
namespace TeamRad\Form\Fieldsets;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
use TeamRad\Form\Fieldsets\Field as Field;

/**
 * The Address class extends FieldSet. It simplifies the 
 * creation of an address FieldSet object by creating the 
 * required fields. These fields' ids can be prefixed by
 * passing $prefix when the Address object is instantiated. 
 */
class Address extends \TeamRad\Form\FieldSet {
	public function __construct($prefix="") {
		$ucprefix = ucwords($prefix);
		// Set and format the label
		$label = 'Address';
		if ($ucprefix) 
			$label = $ucprefix.' '.$label;
		$this->set_label('', $label);
		// Format prefix for ids
		$prefix = prefix($prefix);
		// Add fields
		$this->add_field(new Field\Address1($prefix));
		$this->add_field(new Field\Address2($prefix));
		$this->add_field(new Field\Suburb($prefix));
		$this->add_field(new Field\Postcode($prefix));
		$this->add_field(new Field\State($prefix));
		parent::__construct();

	} // end __construct()
} // end class 	
?>	
