<?php
namespace TeamRad\Form\Fieldsets;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;

/**
 * This class extends FieldSet. It provides a fieldset
 * which can be used for testing the type validation for
 * text inputs. Each field label indicates the type it is
 * testing for. 
 */
class TestTypes extends \TeamRad\Form\Fieldset {
	public function __construct() {
		$this->set_label('', 'Type Validation Tests');
//'min_value',
		$this->set_field('email');
		$this->set_type('email','email');
		$this->set_message('email', 'Please enter a valid email address.');
//'max_value',
		$this->set_field('url');
		$this->set_type('url','url');
		$this->set_message('url', 'Please enter a valid url.');
//'is_positive',
		$this->set_field('int');
		$this->set_type('int','int');
		$this->set_message('int', 'Please enter a valid integer.');
//'is_negative',
		$this->set_field('float');
		$this->set_type('float','float');
		$this->set_message('float', 'Please enter a valid decimal number.');
//'min_length',
		$this->set_field('date');
		$this->set_type('date','date');
		$this->set_message('date', 'Please enter a date in "DD/MM/YYYY" format.');
//'max_length',
		$this->set_field('numeric');
		$this->set_type('numeric','numeric');
		$this->set_message('numeric', 'Please enter a numeric string.');
//'exact_length',
		$this->set_field('text_only');
		$this->set_type('text_only','text_only');
		$this->set_message('text_only', 'Please enter alphabetic characters with no spaces.');
//'str_contains',
		$this->set_field('text_spaces');
		$this->set_type('text_spaces','text_spaces');
		$this->set_message('text_spaces', 'Please enter alphabetic characters with spaces.');
//'str_not_contains'
		$this->set_field('alphanum_only');
		$this->set_type('alphanum_only','alphanum_only');
		$this->set_message('alphanum_only', 'Please enter alphanumeric characters with no spaces.');
//'str_not_contains'
		$this->set_field('alphanum_spaces');
		$this->set_type('alphanum_spaces','alphanum_spaces');
		$this->set_message('alphanum_spaces', 'Please enter alphanumeric characters with spaces.');
		// Call the parent constructor.
		parent::__construct();
	} // end __construct()
} // end class 	
?>	
