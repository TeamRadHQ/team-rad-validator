<?php
namespace TeamRad\Form\Fieldsets;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
use TeamRad\Form\Fieldsets\Field as Field;

/**
 * The ContactInfo class extends FieldSet. It creates 
 * a fieldset for storing contact information. There are
 * two telephone and email fields respectively, and a field
 * for a web address. By default, only the first phone and 
 * email field are required, all others optional.
 */
class ContactInfo extends \TeamRad\Form\FieldSet {
	public function __construct($prefix="") {
		$ucprefix = ucwords($prefix);
		// Set and format the label
		$label = 'Contact Information';
		if ($ucprefix) 
			$label = $ucprefix.' '.$label;
		$this->set_label('', $label);
		// Format prefix for ids
		$prefix = prefix($prefix);
		// Add fields
		$phone1 = new Field\Phone($prefix.'1');
		$phone1->set_label('Primary Phone');
		$phone2 = new Field\Phone($prefix.'2');
		$phone2->set_label('Secondary Phone');
		$phone2->set_required(false);
		$email1 = new Field\Email($prefix.'1');
		$email1->set_label('Primary Email');
		$email2 = new Field\Email($prefix.'2');
		$email2->set_label('Secondary Email');
		$email2->set_required(false);
		$url = new Field\Url($prefix);
		$url->set_required(false);
		$this->add_field($phone1);
		$this->add_field($phone2);
		$this->add_field($email1);
		$this->add_field($email2);
		$this->add_field($url);
		
		parent::__construct();

	} // end __construct()
} // end class 	
?>	
