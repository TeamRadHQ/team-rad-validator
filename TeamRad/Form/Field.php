<?php
namespace TeamRad\Form;

/**
 * The Field class defines a series of properties and methods for storing
 * information about a data field. It is designed to be used by other 
 * objects to collect and display data. 
 */
class Field {
	/**
	 * A list of valid data types which can be set for this field.
	 * Allowed types include: 'text', 'select', 'checkbox', 'radio', 
	 * 'email', 'url', 'int','float', 'date', 'numeric', 'text_only', 
	 * 'text_spaces', 'alphunm_only' and 'alphanum_spaces'
	 * @var array
	 */
	public static $valid_types = ['text', 'select', 'checkbox', 'radio', 'email', 'url', 'int','float', 'date', 'numeric', 'text_only', 'text_spaces', 'alphunm_only', 'alphanum_spaces'];
	/**
	 * A list of valid conditions which can be used to create validation rules.
	 * Valid conditions include: 'min_value', 'min_value', 'max_value', 'is_positive', 
	 * 'is_negative', 'min_length', 'max_length', 'exact_length', 'str_contains' and 
	 * 'str_not_contains'
	 * @var array
	 */
	public static $valid_conditions = ['min_value', 'min_value','max_value','is_positive','is_negative','min_length','max_length','exact_length','str_contains','str_not_contains'];
	/**
	 * A list of valid numeric data types which are valid for testing numeric conditions.
	 * Valid numeric types include: 'int','float' and 'numeric'.
	 * @var array
	 */
	public static $valid_numeric = ['int','float', 'numeric'];
	/** @var  string $id 		A unique id for this field. */
	protected $id; 
	/** @var boolean $required 	Set to true if this is a required field.  */
	protected $required;
	/**
	 * Indicates the type of data this field contains.
	 * @see Field::$valid_types Field::$valid_types 
	 * @var string
	 */
	protected $type;
	/** @var string $label 		A display label for the field. If nothing is supplied the $id is parsed in a  human friendly format.*/
	protected $label;
	/** @var string $placeholder Some placeholder text for the field. If nothing is supplied then the $label is formatted  to lowercase.*/
	protected $placeholder;
	/** @var string $message 	A message providing user information about the field. (eg "Enter date in DD/MM/YYYY format.")*/
	protected $message;
	/** @var string $value 		The field's data value. This will either be a string, or an array if the field can contain multiple values (eg checkbox) */
	protected $value;
	protected $values = array();
	/** @var array $options 	An array of possible options for the fields with a $type like "select", "checkbox" and "radio". */
	protected $options = array();
	/** @var array $conditions 	An array of conditions which are used for validating the field's value. */
	/**
	 * An array of conditions which are used for validating a field's value.
	 * @see Field::$valid_conditions Field::$valid_conditions
	 * @var array
	 */
	protected $conditions = array();
	/** @var array $errors 		A numeric array which stores any validation error messages received for this field's $value.*/
	protected $errors;

	/**
	 * Constructs a new Field object. 
	 * An Id must be passed at construction. However, all other 
	 * arguments are optional. 
	 * @uses Field::set_id() to set field's id.
	 * @see Field::set_id() Field::set_id()
	 * @uses Field::set_required() to set if the field is required.
	 * @see Field::set_required() Field::set_required()
	 * @uses Field::set_type() to set the field's data type.
	 * @see Field::set_type() Field::set_type()
	 * @uses Field::set_label() to set and format the field's label.
	 * @see Field::set_label() Field::set_label()
	 * @uses Field::set_placeholder() to set and format the field's placeholder text.
	 * @see Field::set_placeholder() Field::set_placeholder()
	 * @uses Field::set_message() to set a user message for the field.
	 * @see Field::set_message() Field::set_message()
	 * @uses Field::set_conditions() to set a user conditions for the field.
	 * @see Field::set_conditions() Field::set_conditions()
	 * @uses Field::set_options() to set a user options for the field.
	 * @see Field::set_options() Field::set_options()
	 * @param string  $id          (Required) A unique identifier for this field
	 * @param boolean $required    Defaults to true, which indicates that the field is required.
	 * @param string  $type        The field's data type. Defaults to "text".
	 * @param string  $label       A label for this field. 
	 * @param string  $placeholder An input placeholder for this field.
	 * @param array   $conditions  An array of conditions for data validation.
	 * @param array   $options     An array of option value pairs for selection.
	 * @param string  $message     An information message about this field to display to the user.
	 */
	public function __construct(
		$id, $required=true, $type='text', 
		$label=null, $placeholder=null, 
		$conditions=array(), $options=array(), 
		$message=null 
	) {
		$this->set_id($id);
		$this->set_required($required);
		$this->set_type($type);
		$this->set_label($label);
		$this->set_placeholder($placeholder);
		$this->set_message($message);
		if($conditions)
			$this->set_conditions($conditions);
		if ($options)
			$this->set_options($options);
		$this->set_POST_value();
	}
	/**
	 * Outputs the field's properties in an HTML table. 
	 * @return string A table containing a list of the field's properties.
	 */
	public function __toString() {
		$string ="<table border=1><tr><td colspan=2><h2>Field::".strtoupper($this->id())."</h2></td></tr>";
		$string.="<tr><td>id:</td><td>".$this->id();
		$string.="</td></tr><tr><td>required:</td><td>".($this->required()?"required":"not required")."</td></tr>";
		$string.="<tr><td>type:</td><td>".$this->type()."</td></tr>";
		$string.="<tr><td>label:</td><td>".$this->label()."</td></tr>";
		$string.="<tr><td>placeholder:</td><td>".$this->placeholder()."</td></tr>";
		$string.="<tr><td>message:</td><td>".$this->message()."</td></tr>";
		if($this->conditions) {
			$string.="<tr><td><h4>Condition<h4></td><td><h4>Value<h4></td></tr>";
			foreach($this->conditions as $condition => $value) {
				$string.= "<tr><td>$condition</td><td>$value";
			} // end foreach($condition)
		} // end if($conditions)
		if($this->options) {
			$string.="<tr><td><h4>Option<h4></td><td><h4>Value<h4></td></tr>";
			foreach($this->options as $option => $value) {
				$string.= "<tr><td>".$this->options[$option]["option"]."</td>";
				$string.= "<td>".$this->options[$option]["value"]."<br>";
			} //end foreach($option)
		} //end if($options)
		$string.="</table>";
		return $string;
	} // end __toString();
	/**
	 * Returns the field contents as an array
	 * @return array An array containing the field data.
	 */
	public function get() {
		$field = [
			"id" 			=> $this->id,
			"required" 		=> $this->required,
			"type" 			=> $this->type,
			"label"			=> $this->label,
			"placeholder"	=> $this->placeholder,
		];
		if($this->message)
			$field["message"] = $this->message;
		if($this->value) 
			$field["value"] = $this->value;
		if($this->conditions)
			$field["conditions"] = $this->conditions;
		if($this->options)
			$field["options"]	 = $this->options;
		if(count($this->errors))
			$field["errors"]	 = $this->errors;
		switch($this->type) {
			case 'checkbox':
				$field["is_checkbox"] = true;
				break;
			case 'radio':
				$field["is_radio"] = true;
				break;
			case 'select':
				$field["is_select"] = true;
				break;
			default:
				$field["is_text"] = true;
				break;
		} // end switch($type)
		return $field;
	}
	/**
	 * Gets the field's id
	 * @see Field::$id Field::id
	 * @return 	String 	The id of the field
	 */
	public function id() {
		return $this->id;
	}
	/**
	 * Returns true if the field is required.
	 * @see Field::$required Field::required
	 * @return 	Boolean True indicates that the field is required.
	 */	
	public function required() {
		return $this->required;
	}
	/**
	 * Gets the field's type.
	 * @see Field::$type Field::type
	 * @return 	String 	The data type of the field.
	 */	
	public function type() {
		return $this->type;
	}
	/**
	 * Gets the field's value or values.
	 * @see Field::$value Field::value
	 * @return 	String 	The value of the field.
	 */
	public function value() {
		if ($this->value && !$this->values)
			return $this->value;
		if (!$this->value && $this->values)
			return $this->values;
	}
	/**
	 * Gets the field's value or values and returns it as an array.
	 * @see Field::$value Field::value
	 * @return 	Array 	An array of values or value.
	 */
	public function values() {
		if ($this->value && !$this->values) {
			$array  = array($this->value);
			return $array;
		}
		if ($this->values)
			return $this->values;
	}

	/**
	 * Gets the field's label
	 * @see Field::$label Field::label
	 * @return 	String 	A label for the field.
	 */
	public function label() {
		return $this->label;
	}
	/**
	 * Gets the field's placeholder.
	 * @see Field::$placeholder Field::placeholder
	 * @return 	String 	A placeholder for the field.
	 */
	public function placeholder() {
		return $this->placeholder;
	}
	/**
	 * Gets the field's message
	 * @see Field::$message Field::message
	 * @return String 	A message for the field. 
	 */
	public function message() {
		return $this->message;
	}
	/**
	 * Gets the field's options and values
	 * @see Field::$options Field::options
	 * @return Array 	An array of option value pairs for the field.
	 */
	public function get_options() {
		return $this->options;
	}
	/**
	 * Gets the field's validation conditions. 
	 * @see Field::$conditions Field::conditions
	 * @return Array 	An array of validation conditions and their values.
	 */
	public function get_conditions() {
		return $this->conditions;
	}
	/**
	 * Gets the field's errors. 
	 * @see Field::$errors Field::errors
	 * @return Array 	An array of validation errors received for this field. 
	 */
	public function get_errors() {
		return $this->errors;
	}
	public function add_error($message) {
		$this->errors[]["message"] = $message;
	}
	/**
	 * Sets the field's id.
	 * @see Field::$id Field::id
	 * @param String $id 	The id for the field.
	 */
	public function set_id($id) {
		$this->id=$id;
	}
	/**
	 * Sets the field as required or not.
	 * Defaults to true.
	 * @see Field::$required Field::required
	 * @param boolean $required True if this is a required field.
	 */
	public function set_required($required=true) {
		$this->required=$required;
	}
	/**
	 * Sets the field's type. 
	 * Defaults to 'text' if $type is not in $valid_types.
	 * @todo List all types, add switch statement.
	 * @see Field::$type Field::type
	 * @see Field::$valid_types Field::$valid_types
	 * @param String $type The data type of the field.
	 */
	public function set_type($type='text') {
		if(!in_array($type, self::$valid_types))
			$type = 'text';
		$this->type=$type;
	}
	/**
	 * Sets the field's value or values. 
	 * If a field can only accept one value, this will be 
	 * stored in $value.
	 * Otherwise, if there are multiple values, these will 
	 * be stored $values
	 * @see Field::$value Field::value
	 * @see Field::$values Field::values
	 * @param Mixed 	$value 	The value as a string, or an array of values. 
	 */
	public function set_value($value) {
		if(is_array($value)) {
			$this->values=$value;
			if(count($value))
				$this->value=1;
		} else {
			$this->value=$value;
		}
		$this->set_selected();
	}
	/**
	 * Checks the $_POST array and if there is a key for this 
	 * field's id, it's value is set. 
	 * If the field type is 'checkbox' then set_POSTcheckbox()
	 * is run to set the field values. 
	 * @see Field::set_POST_checkbox() Field::set_POST_checkbox()
	 */
	public function set_POST_value() {
		$value=null;
		if ($this->type == "checkbox")
			$value = $this->set_POST_checkbox();
		if(isset($_POST[$this->id]))
			$value = $_POST[$this->id];
		$this->set_value($value);
	}
	/**
	 * Checks the $_POST array for any checked values for this 
	 * field. Each form input for checkbox fields should have id's
	 * equal to [$fieldname]_[$value].
	 */
	private function set_POST_checkbox() {
		$value=[];
		foreach($this->options as $option) {
			$id = $this->id.'_'.$option["value"];
			if (isset($_POST[$id])) 
				$value[]=$_POST[$id];
		} // end foreach()
		return $value;
	}// end set_POST_checkbox
	/**
	 * Checks each option in $options and compares its value to 
	 * $value and $values. If a match is found, the "is_selected"
	 * key is added to the option. 
	 * @see Field::$options Field::$options
	 * @see Field::$value Field::$value
	 * @see Field::$values Field::$values
	 */
	private function set_selected() {
		if (!($this->options)) // Kill if there are no options
			return; 
		// Loop through the options and set selected ones.
		foreach($this->options as $key => $option) {
			if($option["value"] === $this->value || in_array($option["value"], $this->values)) 
				$this->options[$key]["selected"] = true;
		} // end foreach
	} // end set_selected()
	/**
	 * Sets a label for the field. 
	 * If none is passed, the field's id is formatted to be readable. 
	 * eg: "this_id" will be parsed as "This Id".
	 * @param String $label A label for the field.
	 * @see Field::$label Field::$label
	 * @see Field::label() Field::label()
	 */
	public function set_label($label=null) {
		if ($label) { // A label has been passed
			$this->label=$label;
		} else { // Set the label using $id 
			$this->label = str_replace('_', ' ', $this->id);
		} // Then capitalise firs letters of words
		$this->label = ucwords($this->label); 
	} // end set_label()
	/**
	 * Sets a placeholder for the field. 
	 * If none is passed, the field's label is parse as the placeholder.
	 * eg: "This Id" will be parsed as "This id...".
	 * @param String $placeholder A placeholder for the field.
	 * @see Field::$placeholder Field::placeholder
	 */
	public function set_placeholder($placeholder=null) {
		if($placeholder) {	
			$this->placeholder=$placeholder;
		} else {
			$this->placeholder = strtolower($this->label."...");
		}
		$this->placeholder = ucfirst($this->placeholder);
	} // end set_placehoder()
	/**
	 * Sets a message to provide the user information about this field.
	 * @see Field::$message Field::message
	 * @param String $message A message with information about this field.
	 */
	public function set_message($message) {
		$this->message=$message;
	}
	/**
	 * Sets an array of "option" and "value" pairs for this field. 
	 * This is used for types like "select", "checkbox", and "radio"
	 * where a field's value(s) are set according to a pre-defined list.
	 * @param Array $options An array containing pairs of keys labelled
	 * "option" and "value".
	 * @see Field::set_option() Field::set_option()
	 * @see Field::get_POST_checkbox() Field::get_POST_checkbox()
	 * @see Field::$options Field::options
	 */
	public function set_options(array $options) {
		foreach($options as $option => $value) {
			$this->set_option($options[$option]);
		}
	}
	/**
	 * Adds an "option" "value" pair to the $options array if its value 
	 * does not already exist.
	 * @param array $option An array containing "value" and "option". 
	 * @see Field::set_option() Field::set_option()
	 * @see Field::$options Field::options
	 */
	public function set_option(array $option) {
		if(!isset($option["option"]) && !isset($option["value"])) {
			return; // This is not a valid option value pair.
		} else {  // If valid, reset the array to only contain these.
			$option = [
				"option" => $option["option"],
				"value" => $option["value"]
			];
		}
		// Loop through the options this option doesn't exist. 
		foreach($this->options as $test => $values) {
			if($this->options[$test] == $option) // This option exists
				return;
		}
		// If it's made it this far, add it to the options array.
		$this->options[] = $option;
	}

	/**
	 * Takes an array of conditions and adds them to the field's $conditions
	 * array. 
	 * @param Array $conditions Conditions and their values.
	 * @see Field::set_condition() Field::set_condition()
	 * @see Field::$conditions Field::$conditions
	 */
	public function set_conditions(array $conditions) {
		foreach($conditions as $condition => $value) {
			$this->set_condition($condition, $value);
		}
	}
	/**
	 * Adds or overwrites any valid condition in the $conditions array.
	 * @param string $condition The condition you want to set.
	 * @param mixed $value     	The numeric or string value you want to set.
	 * @see Field::set_conditions() Field::set_conditions()
	 * @see Field::$conditions Field::$conditions
	 */
	public function set_condition($condition, $value) {
		if(!in_array($condition, self::$valid_conditions))
			return;
		$this->conditions[$condition] = $value;
	}
	/**
	 * Returns true if this field's type is a valid numeric.
	 * @return [type] [description]
	 */
	private function vld_numeric() {
		if(in_array($this->type, self::$valid_numeric)) 
			return true;
	}
	/**
	 * Set a minimum allowed (numeric) value ($val) for this field. 
	 * @param  int|float $val The minimum value allowed for this field.
	 * @see Field::cnd_maxval() Field::cnd_maxval()
	 * @see Field::cnd_btwval() Field::cnd_btwval()
	 * @see Field::$conditions Field::$conditions
	 */
	public function cnd_minval($val) {
		if(!$this->vld_numeric())
			return;
		$this->set_condition("min_value", $val);
	}
	/**
	 * Set the maximum (numeric) allowed value ($val) for this field.
	 * @param  int|float $val The minimum value for this field.
	 * @see Field::cnd_minval() Field::cnd_minval()
	 * @see Field::cnd_btwval() Field::cnd_btwval()
	 * @see Field::$conditions Field::$conditions
	 */
	public function cnd_maxval($val) {
		if(!$this->vld_numeric())
			return;
		$this->set_condition("max_value", $val);
	}
	/**
	 * Set a maximum and minimum (numeric) value ($val1, $val2) for this
	 * field. These can be passed in any order.
	 * @param  int|float $val1 The first value you want to set between.
	 * @param  int|float $val2 The second value you want to set between.
	 * @see Field::cnd_minval() Field::cnd_minval()
	 * @see Field::cnd_maxval() Field::cnd_maxval()
	 * @see Field::$conditions Field::$conditions
	 */
	public function cnd_btwval($val1, $val2) {
		if(!$this->vld_numeric())
			return;
		$vals = minmax($val1, $val2);	
		$this->cnd_minval($vals["min"]);
		$this->cnd_maxval($vals["max"]);
	}
	/**
	 * Specify that a field's numeric value must be positive.
	 * @param  boolean $pos	True if this field must be positive. 
	 * @see Field::$conditions Field::$conditions
	 * @see Field::cnd_pos() Field::cnd_pos()
	 */	
	public function cnd_pos($pos=true) {
		if(!$this->vld_numeric())
			return;
		$this->set_condition("is_positive", $pos);
	}
	/**
	 * Specify that a field's numeric value must be negative.
	 * @param  boolean	$neg True if this field must be negative. 
	 * @see Field::cnd_pos() Field::cnd_pos()
	 * @see Field::$conditions Field::$conditions
	 */	
	public function cnd_neg($neg=true) {
		if(!$this->vld_numeric())
			return;
		$this->set_condition("is_negative", $neg);
	}
	/**
	 * Set the exact length of this field
	 * @param int $val 	The exact length of this field's value.  
	 * @see Field::cnd_minlen() Field::cnd_minlen()
	 * @see Field::cnd_maxlen() Field::cnd_maxlen()
	 * @see Field::cnd_btwlen() Field::cnd_btwlen()
	 * @see Field::$conditions Field::$conditions
	 */	
	public function cnd_len($val) {
		$this->set_condition("exact_length", $val);
	}
	/**
	 * Set the minimum length allowed for this field
	 * @param int $val 	The minimum length allowed for this field's value.  
	 * @see Field::cnd_len() Field::cnd_len()
	 * @see Field::cnd_maxlen() Field::cnd_maxlen()
	 * @see Field::cnd_btwlen() Field::cnd_btwlen()
	 * @see Field::$conditions Field::$conditions
	 */	
	public function cnd_minlen($val) {
		$this->set_condition("min_length", $val);
	}
	/**
	 * Set the maximum length allowed for this field. 
	 * @param int $val 	The minimum length allowed for this field's value.  
	 * @see Field::cnd_len() Field::cnd_len()
	 * @see Field::cnd_minlen() Field::cnd_minlen()
	 * @see Field::cnd_btwlen() Field::cnd_btwlen()
	 * @see Field::$conditions Field::$conditions
	 */	
	public function cnd_maxlen($val) {
		$this->set_condition("max_length", $val);
	}
	/**
	 * Set the minimum and maximum length allowed for this field. 
	 * @param int $val1 	The first value to set between.  
	 * @param int $val2 	The second value to set between.  
	 * @see Field::cnd_len() Field::cnd_len()
	 * @see Field::cnd_minlen() Field::cnd_minlen()
	 * @see Field::cnd_maxlen() Field::cnd_maxlen()
	 * @see Field::$conditions Field::$conditions
	 */	
	public function cnd_btwlen($val1, $val2) {
		$vals = minmax($val1, $val2);	
		$this->cnd_minlen($vals["min"]);
		$this->cnd_maxlen($vals["max"]);
	}
	/**
	 * Set a string that this field's value must contain.
	 * @param string $val 	A string which this field must contain.  
	 * @see Field::cnd_not() Field::cnd_not()
	 * @see Field::$conditions Field::$conditions
	 */	
	public function cnd_contains($val) {
		$this->set_condition("str_contains", $val);
	}
	/**
	 * Set a string that this field's value must not contain.
	 * @param string $val 	A string which this field must not contain.  
	 * @see Field::cnd_contains() Field::cnd_contains()
	 * @see Field::$conditions Field::$conditions
	 */	
	public function cnd_not($val) {
		$this->set_condition("str_not_contains", $val);
	}
}
?>
