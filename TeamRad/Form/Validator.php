<?php
namespace TeamRad\Form;
use TeamRad\Helpers\Vld as Vld;

/**
 * The Validator class takes a fieldset, validates its
 * field input values against requirement, type and 
 * any validation conditions that have been set. Any
 * validation errors are added to their respective fields
 * and returned using the get_fieldset() method.
 */
class Validator {
	protected $errors = array();
	protected $fieldset = array();

	/**
	 * Sets the fieldset and runs validate_fields()
	 * @param [type] $fieldset [description]
	 * @see Validator::$fieldset
	 * @see Validator::validate_fields()
	 */
	public function __construct($fieldset) {
		$this->fieldset = $fieldset;
		$this->validate_fields();
	}
////////BEGIN GETTER/SETTER FUNCTIONS////////////////	 
	/**
	 * Returns the fieldset to the user.
	 * @return array An array of field objects.
	 */
	public function get_fieldset() {
		return $this->fieldset;
	}
	/**
	 * Returns a count of validation errors.
	 * @return int The number of validation errors that were found.
	 */
	public function error_count() {
		return count($this->errors);
	}
	/**
	 * Adds an error to the validator array and runs the 
	 * field's add_error method.
	 * @param \TeamRad\Form\Field $field   A Field object
	 * @param string $message The validation error message to be set for this field.
	 */
	private function add_error($field, $message) {
		$field->add_error($message);
		$error = array(
			'id'		=> $field->id(),
			'message'	=> $message,
		);
		$this->errors[] = $error;
	}
////////END GETTER/SETTER FUNCTIONS////////////////
////////BEGIN VALIDATION FUNCTIONS////////////////
	/**
	 * Loops through the $fields array and validates each field using 
	 * The vld_field() method. 
	 */
	protected function validate_fields() {
		foreach($this->fieldset as $field) { // Loop through each field
			$id = $field->id(); // Get the field_id
			$this->vld_field($field);
			// var_dump($this->fieldset);
		}// End foreach
	}
	/**
	 * Validates an individual field against requirement, type and 
	 * conditions. 
	 * If a field does not pass requirement or is empty, validation 
	 * stops. Otherwise, a field is checked against its type. If type
	 * validation is passed, then the field is checked against any of
	 * its conditions.
	 * @param  String $field_id The id of the field being validated.
	 */
	private function vld_field($field) {
		if (!$this->required($field)) { // Check required or pass
		} else { // Requirements passed 
			if ($this->vld_types($field)) // Validate its type
				$this->vld_conditions($field); // Validate its conditions
		} // End if..else		
	} // End vld_field()
	/**
	 * Validates a field against its type and adds an error if its value
	 * fails validation, or returns true if it passes.
	 * @param  Array 	$field 	The field being tested.
	 * @return Array 	$errors Adds an error to the $errors aray if type validation fails.
	 * @return boolean          Returns true if the field passes type validation.
	 */
	private function vld_types($field) {
		if (empty($field->value() ))
			return true;
		$error_count = $this->error_count(); // Check current error count
		switch($field->type()){ // Do type validation for field types
			case 'email':
				$this->email($field); 	break;
			case 'url':
				$this->url($field);		break;
			case 'int':
				$this->int($field);		break;
			case 'float':
				$this->float($field);		break;
			case 'date':
				$this->date($field); 	break;
			case 'numeric':
				$this->numeric($field);		break; 
			case 'text_only':
				$this->txt_only($field);	break; 
			case 'text_spaces':
				$this->txt_spc($field);		break;
			case 'alphanum_only':
				$this->alnum($field);		break;
			case 'alphanum_spaces':
				$this->alnum_spc($field);	break; 
		} // End Switch
		if ($this->error_count() == $error_count) 
			return true; // There are no errors
	}

	/**
	 * Validates a field against its conditions. If there are no
	 * conditions, then the field passes conditional validation. 
	 * Otherwise, each condition is checked individually. If conditional
	 * errors are found, these are added to the $errors array. Otherwise,
	 * the field passes conditional validation.
	 * @param  Array 	$field 	The field being tested.
	 * @return Boolean       	True if there are no conditional errors.
	 */
	private function vld_conditions($field) {
		if ( !$field->get_conditions() || empty($field->value() ))
			return true; // If there are no conditions set
		$error_count = $this->error_count();
		foreach(array_keys($field->get_conditions()) as $condition) {
			switch($condition) { // Test each condition and set the appropriate error $message
				case 'min_value':
					$this->minval($field);		break;
				case 'max_value':
					$this->maxval($field); 		break;
				case 'is_positive':
					$this->ispos($field);		break;
				case 'is_negative':
					$this->isneg($field);		break;
				case 'min_length':
					$this->minlen($field);		break;
				case 'max_length':
					$this->maxlen($field);		break;
				case 'exact_length':
					$this->exlen($field);		break;
				case 'str_contains':
					$this->contains($field);	break;
				case 'str_not_contains':
					$this->not_contains($field);break;
			} // End Switch($condition)
		} // End Foreach
		if ($this->error_count() == $error_count) 
			return true; // There are no errors
	} // end $this->vld_conditions
////////END VALIDATION FUNCTIONS////////////////
////////BEGIN REQUIRE VALIDATION////////////////
	/**
	 * Checks if a field is required. If it is required and is 
	 * empty an error is added. If any field is empty, this method 
	 * returns false to indicate that no further validation should be
	 * done. 
	 * @param  Array 	$field 	The field to be tested
	 * @return Boolean        	Returns true if the field is not empty.
	 */
	private function required($field) {
		// If this field fails requirement
		if(!Vld::required($field)) { 
			switch($field->type()) {
				case "select":
				case "radio":
					$this->add_error($field, "Please make a selection for ". $field->label().".");		
					return false;
				case "checkbox":
					$this->add_error($field, "Please make at least one selection for ". $field->label().".");		
					return false;
				default:
					$this->add_error($field, $field->label()." is required and cannot be left blank.");
					return false;
			} // End switch(type)
		}//End if Vld::required()
		return true;
	}	
////////END IS REQUIRED FUNCTIONS////////////////
	/**
	 * Checks a field's value and adds an error to the $errors array
	 * if it is not an integer.
	 * @param  Array  	$field 		The field being tested.
	 */
	private function int($field) {
		if (!Vld::int($field)) 
			$this->add_error($field, "Please enter a whole number for ".$field->label().".");
	} // end int()
	/**
	 * Checks a field's value and adds an error to the $errors array 
	 * if it is not a float.
	 * @param  Array 	$field 	The field being tested.
	 */
	private function float($field) {
		if (!Vld::float($field)) 
			$this->add_error($field, "Please enter a number for ". $field->label().".");
	} // end float()
	/**
	 * Checks a field's value and adds an error to the $errors array 
	 * if it is not a valid email address.
	 * @param  Array 	$field 	The field being tested.
	 */
	private function email($field) {
		if (!Vld::email($field)) 
			$this->add_error($field, "Please enter a valid email address for ".$field->label().".");		
	}
	/**
	 * Checks a field's value and adds an error to the $errors array 
	 * if it is not a valid url.
	 * @param  Array 	$field 	The field being tested.
	 */
	private function url($field) {
		if (!Vld::url($field)) 
			$this->add_error($field, "Please enter a valid web address for ".$field->label().".");
	}
	/**
	 * Checks a field's value and adds an error to the $errors array 
	 * if it is not a valid date which is explicitly formatted as:
	 * "DD/MM/YYYY".
	 * @param  Array 	$field 	The field being tested.
	 */
	private function date($field) {
	$date = $field->value(); $is_date = false;
		if (!Vld::date($field)) 
			$this->add_error($field, "Please enter a valid date for ".$field->label().".");
	} // End is_date()
		/**
	 * Checks a field's value and adds an error to the $errors array 
	 * if it is not numeric. This can be use for long numeric strings
	 * such as student or phone numbers.
	 * @param  Array 	$field 	The field being tested.
	 */
	private function numeric($field) {
		if (!Vld::numeric($field)) 
			$this->add_error($field, $field->label()." must be numeric.");
	}
	/**
	 * Adds an error if a string contains anything other than 
	 * non-spaced alphabetic characters.
	 * @param  Array 	$field 	The field being tested.
	 */
	private function txt_only($field) {
		if (!Vld::txt_only($field)) 
			$this->add_error($field, $field->label()." must contain letters only.");
	}
	/**
	 * Adds an error if a field contains anything other than
	 * alphabetic characters and spaces.
	 * @param  Array 	$field 	The field being tested.
	 */
	private function txt_spc($field) {
		// Strip spaces from the string and run $this->txt_only()
		if (!Vld::txt_spc($field)) 
			$this->add_error($field, $field->label()." must contain letters and spaces only.");
	}
	/**
	 * Adds an error if a field contains anything other than
	 * alphanumeric characters.
	 * @param  Array 	$field 	The field being tested.
	 */
	private function alnum($field) {
		if (!Vld::alnum($field))
			$this->add_error($field, $field->label()." must contain alphanumeric characters only.");			
	}
	/**
	 * Adds an error if a field contains anything other than
	 * alphanumeric characters and spaces.
	 * @param  Array 	$field 	The field being tested.
	 */
	private function alnum_spc($field) {
		$value = $field->value();
		if (!Vld::alnum_spc($field)) 
			$this->add_error($field, $field->label()." must contain alphanumeric characters and spaces only.");
	}// End alphanum_spaces()
////////END TYPE VALIDATION////////////////
////////BEGIN CONDITIONAL VALIDATION////////////////
	/**
	 * Gets the value for the field condition you want to test.
	 * @param  Array 	$field      The field you're testing
	 * @param  String 	$condition 	The condition whose value you want to get
	 * @return String   	        The test value for the $condtion
	 */
	private function get_condition($field, $condition) {
		$conditions = $field->get_conditions();
		return $conditions[$condition];
	}
	/**
	 * Adds an error if the field's minimum value is less than 
	 * its condition.
	 * @param  Array 	$field      The field you're testing
	 */
	private function minval($field) {
		$condition = $this->get_condition($field, "min_value");
		$value = $field->value();
		if (!Vld::minval($field)) 
			$this->add_error($field, $field->label()." must be greater than or equal to $condition.");
	} // end min_value()
	/**
	 * Adds an error if the field's maximum value is greater
	 * than or equal to its condition.         		
	 * @param  Array 	$field      The field you're testing
	 */
	private function maxval($field) {
		$condition = $this->get_condition($field, "max_value");
		$value = $field->value();
		if (!Vld::maxval($field)) 
			$this->add_error($field, $field->label()." must be less than or equal to {$condition}.");
	}
	/**
	 * Adds an error if a field's value is negative
	 * @param  Array 	$field      The field you're testing
	 */
	private function ispos($field) {
		if (!Vld::ispos($field)) 
			$this->add_error($field, $field->label()." must be a positive value.");
	}
	/**
	 * Adds an error if a field's value is positive
	 * @param  Array 	$field      The field you're testing
	 */
	private function isneg($field) {
		if (!Vld::isneg($field)) 
			$this->add_error($field, $field->label()." must be a negative value.");
	}
	/**
	 * Adds an error if a field's length is shorter than 
	 * its condition.
	 * @param  Array 	$field      The field you're testing
	 */
	private function minlen($field) {
		if(!Vld::minlen($field)) {
			$condition = $this->get_condition($field, "min_length");
			$this->add_error($field, $field->label()." must be at least {$condition} characters long.");			
		}
	}
	/**
	 * Adds an error if a field's length is longer than 
	 * its condition.
	 * @param  Array 	$field      The field you're testing
	 */
	private function maxlen($field) {
		if(!Vld::maxlen($field)) {
			$condition = $this->get_condition($field, "max_length");
			$this->add_error($field, $field->label()." must be no more {$condition} characters long.");
		}
	}
	/**
	 * Adds an error if a field's length is not equal to 
	 * its condition.
	 * @param  Array 	$field      The field you're testing
	 */
	private function exlen($field) {
		if(!Vld::exlen($field)) {
			$condition = $this->get_condition($field, "exact_length");
			$this->add_error($field, $field->label()." must be no exactly {$condition} characters long.");
		}
	}
	/**
	 * Adds an error if the field does not contain its 
	 * conditional string
	 * @param  Array 	$field      The field you're testing
	 */
	private function contains($field) {
		if(!Vld::contains($field)) {
			$sub_string = $this->get_condition($field, "str_contains");
			$this->add_error($field, $field->label()." must contain '$sub_string'.");
		} 
	}
	/**
	 * Adds an error if the field contains its conditional string.
	 * @param  Array 	$field      The field you're testing
	 */
	private function not_contains($field) {
		if(!Vld::not_contains($field)) {
			$sub_string = $this->get_condition($field, "str_not_contains");
			$this->add_error($field, $field->label()." must contain '$sub_string'.");
		}
	}	
////////END CONDITIONAL VALIDATION////////////////
}// Class End
?>

