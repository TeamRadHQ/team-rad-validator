# Team Rad Validator

Team Rad Validator is a tool for generating form fields and validating their input. It includes a set of classes and methods for defining forms which contain fields grouped into field sets. 

The back-end handling has been built from the ground up, with the front-end being delivered using [Mustache templates](https://mustache.github.io) and [Bootstrap CSS](http://getbootstrap.com).


### More Information
[Technical Overview](http://portfolio.teamradhq.com/assets/share.it-validator-report.pdf)